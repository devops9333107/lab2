#include <string>
#include <iostream>
#include "countwords.h"
using namespace std;

int CountWords(string str)
{
   bool inSpaces = true;
   int numWords = 0;
   
   for (char& c : str)
   {
      if (c == ' ')
      {
         inSpaces = true;
      }
      else if (inSpaces)
      {
         numWords++;
         inSpaces = false;
      }
   }

   return numWords;
}
