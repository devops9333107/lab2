#include <iostream>
#include <string>
#include "countwords.h"

int main() {
    std::cout << "Введите строку в которой необходимо посчитать слова:" << std::endl;
    std::string str;
    getline(std::cin, str);
    std::cout << "Количество слов в строке: " << CountWords(str) << std::endl;
    return 0;
}
