#include <cassert>
#include <iostream>
#include "countwords.h"

int main() {
    // Тест с одним словом
    assert(CountWords("Word") == 1);
    std::cout << "Тест 1 пройден: одно слово" << std::endl;

    // Тест с обычной строкой
    assert(CountWords("Это тестовая строка") == 3);
    std::cout << "Тест 2 пройден: обычная строка" << std::endl;

    // Тест со строкой, содержащей несколько пробелов подряд
    assert(CountWords("Это   тестовая строка") == 3);
    std::cout << "Тест 3 пройден: строка с несколькими пробелами подряд" << std::endl;

    // Тест с пустой строкой
    assert(CountWords("") == 0);
    std::cout << "Тест 4 пройден: пустая строка" << std::endl;

    std::cout << "Все тесты успешно пройдены!" << std::endl;
    return 0;
}
