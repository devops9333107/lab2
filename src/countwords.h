#ifndef COUNTWORDS_H
#define COUNTWORDS_H

#include <string>

int CountWords(const std::string str);

#endif // COUNTWORDS_H
